# GrainChain #



## Demo ##
[Here](https://bitbucket.org/gersain1990/grainchainios/src/8d1d5d3aaf06/README.md?at=master) (built from *this* readme) and
[here](https://bitbucket.org/gersain1990/grainchainios/src/8d1d5d3aaf06/?at=master) (Main code).


## Features ##
- [GoogleMapsSDK](https://developers.google.com/maps/documentation/ios-sdk/start)


## Installation ##
First install CocoaPods [CocoaPods](https://cocoapods.org/). 

Installation with terminal:


```
$ sudo gem install cocoapods
```


In proyect GrainChainiOS folder type pod initialize


```
$ pod init
```


Open your podfile create and add support for Google Maps SDK


pod 'GoogleMaps', '5.1.0'


For install the Google Maps SDK pod write in terminal


```
$ pod install
```


## Author & License
Proyect created by [Gersain Aguilar Pardo](https://bitbucket.org/gersain1990/)

## Changelog ##
#### Version 1.0 (version.build) ####
- Initial release.